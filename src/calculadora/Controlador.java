package calculadora;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controlador implements ActionListener {

    private Numeros modelo;
    private CalculadoraFRM vista;

    public Controlador(Numeros modelo, CalculadoraFRM vista) {
        this.modelo = modelo;
        this.vista = vista;
    }

    public void iniciar() {
        vista.setTitle("Calculadora");//Titulo de la ventana
        vista.setLocationRelativeTo(null);//Ubicar en el centro de la pantalla
        vista.setVisible(true);//Mostrar
        asignarControl();
    }

    public void Calcular() {

        float Numero1 = Float.parseFloat(vista.getNum1().getText());
        float Numero2 = Float.parseFloat(vista.getNum2().getText());

        modelo.setNumero1(Numero1);
        modelo.setNumero2(Numero2);

    }

    private void asignarControl() {
        vista.getBtnSumar().addActionListener(this);
        vista.getBtnRestar().addActionListener(this);
        vista.getBtnMulti().addActionListener(this);
        vista.getBtnDiv().addActionListener(this);
        vista.getLimpiar().addActionListener(this);
    }
    
    public void Limpiar(){
    
        vista.getNum1().setText("");
        vista.getNum2().setText("");
        vista.getResultado().setText("");
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == vista.getBtnSumar()) {
            Calcular();
            float sum = modelo.Sumar();

            vista.getResultado().setText("" + sum);
        }
        if (ae.getSource() == vista.getBtnRestar()) {
            Calcular();
            float sum = modelo.Resta();

            vista.getResultado().setText("" + sum);
        }
        if (ae.getSource() == vista.getBtnMulti()) {
            Calcular();
            float sum = modelo.Multi();

            vista.getResultado().setText("" + sum);
        }
        if (ae.getSource() == vista.getBtnDiv()) {
            Calcular();
            float sum = modelo.Divi();

            vista.getResultado().setText("" + sum);
        }
        if(ae.getSource() == vista.getLimpiar())
        {
        Limpiar();
        }

    }

}
