/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import java.awt.EventQueue;

/**
 *
 * @author LUIS
 */
public class Calculadora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Ejecutar la aplicación 
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {     
               new Controlador(
                    new Numeros(),new CalculadoraFRM()
              ).iniciar();
            }
        });
    }
    
}
