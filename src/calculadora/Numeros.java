/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

/**
 *
 * @author LUIS
 */
public class Numeros {
    private float Numero1;
    private float Numero2;

    public Numeros() {
    }

    public Numeros(float Numero1, float Numero2) {
        this.Numero1 = Numero1;
        this.Numero2 = Numero2;
    }

    public float getNumero1() {
        return Numero1;
    }

    public void setNumero1(float Numero1) {
        this.Numero1 = Numero1;
    }

    public float getNumero2() {
        return Numero2;
    }

    public void setNumero2(float Numero2) {
        this.Numero2 = Numero2;
    }
    
    public float Sumar(){
    return Numero1 + Numero2;
    }    
    public float Resta(){
    return Numero1 - Numero2;
    }    
    public float Multi(){
    return Numero1 * Numero2;
    }    
    public float Divi(){
    return Numero1 / Numero2;
    }    
}
